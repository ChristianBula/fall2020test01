package utilities;

public class MatrixMethod {
	public static void main(String args[]) {
	    int [][] numbers = new int[2][3];
		numbers[0][0] = 1;
		numbers[0][1] = 2;
		numbers[0][2] = 3;
		numbers[1][0] = 4;
		numbers[1][1] = 5;
		numbers[1][2] = 6;
		
		int[][] duplicate = duplicate(numbers);
		for(int i = 0;i<duplicate.length;i++) {
            for(int j = 0;j<duplicate[0].length;j++){
                System.out.println(duplicate[i][j]);
            }
		}
	}	
    public static int[][] duplicate(int[][] numbers){
    	int[][] numbers2 = new int[numbers.length][numbers[0].length*2];
    	for(int i = 0;i<numbers2.length;i++) {
    		int arrayCount = 0;
    		
    		 for(int j = 0;j<numbers2[0].length;j++) {
    			 
    			 if(arrayCount == numbers[0].length-1) {
    				 numbers2[i][j] = numbers[i][arrayCount];
    			     arrayCount = 0;
    			 }
    			 else {
    				 numbers2[i][j] = numbers[i][arrayCount];
    				 arrayCount++;
    			 }
    		}
    	}
    	return numbers2;
    }
}
