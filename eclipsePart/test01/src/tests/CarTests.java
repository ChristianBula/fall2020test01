package tests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import vehicles.Car;

class CarTests {

	@Test
	void CarTest() {
	    try {
	    	Car c = new Car(-10);
	    }
	    catch(Exception IllegalArgumentException){
	        System.out.println("Value is negative");
	    }
	    Car c = new Car(10);
	    assertEquals(10,c.getSpeed());

	}
	
	@Test
	void getSpeedTest() {
		Car c = new Car(200);
		assertEquals(200,c.getSpeed());
	}
	
	@Test
	void getLocationTest() {
		Car c = new Car(20);
		//Testing that the default value is actually 0;
		assertEquals(0,c.getLocation());
		
		//Testing that the value of location is right if we move right;
		c.moveRight();
		assertEquals(20,c.getLocation());
		
		//Testing that the value of location is right if we move left;
		c.moveLeft();
		assertEquals(0,c.getLocation());
	}
	@Test
	void MoveRightTest() {
		//This test is very similar to the getLocationTest;
		Car c = new Car(15);
		c.moveRight();
		assertEquals(15,c.getLocation());
	}
	
	@Test
	void MoveLeftTest() {
		Car c = new Car(15);
		c.moveLeft();
		assertEquals(-15,c.getLocation());
	}
	
	@Test
	void accelerateTest() {
		Car c = new Car(10);
		c.accelerate();
		assertEquals(11,c.getSpeed());
	}
	
	@Test
	void decelerate() {
		//Test if the car gets to 1;
		Car c = new Car(2);
		c.decelerate();
		assertEquals(1,c.getSpeed());
		
		//Test if the car gets to under 0 if it stays at 0 and not -1;
		c.decelerate();
		c.decelerate();
		assertEquals(0,c.getSpeed());
	}

}
