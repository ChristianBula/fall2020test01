package tests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import utilities.MatrixMethod;

class MatrixMethodTests {

	@Test
	void test() {
		int [][] numbers = new int[][] {{1,2,3},{4,5,6}};
		int [][] numbers2 = MatrixMethod.duplicate(numbers);
		int [][] expectedArray = new int[][] {{1,2,3,1,2,3},{4,5,6,4,5,6}};
		assertArrayEquals(expectedArray,numbers2);
	}

}
